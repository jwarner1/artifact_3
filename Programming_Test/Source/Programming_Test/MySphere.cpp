// Fill out your copyright notice in the Description page of Project Settings.


#include "MySphere.h"

// Sets default values
AMySphere::AMySphere()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Creates the Sphere mesh component
	Sphere = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Sphere"));
	RootComponent = Sphere;
	static ConstructorHelpers::FObjectFinder<UStaticMesh> SphereVisualAsset(TEXT("/Game/Geometry/Meshes/Sphere.Sphere"));
	if (SphereVisualAsset.Succeeded())
	{
		Sphere->SetStaticMesh(SphereVisualAsset.Object);
	}

	//Sets the parameters for the Sphere component
	Sphere->SetSimulatePhysics(true);
	Sphere->SetCollisionProfileName("PhysicsActor");
	Sphere->SetNotifyRigidBodyCollision(true);
}

//Used to call the BP functionality upon receiving a 'Hit'
void AMySphere::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
	CollisionFunc(HitComponent, OtherActor, OtherComponent, NormalImpulse, Hit); //Calls the BP function
}

// Called when the game starts or when spawned
void AMySphere::BeginPlay()
{
	Super::BeginPlay();
	
	Sphere->OnComponentHit.AddDynamic(this, &AMySphere::OnHit);
}

// Called every frame
void AMySphere::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

