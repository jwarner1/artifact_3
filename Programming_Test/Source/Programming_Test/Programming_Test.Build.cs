// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Programming_Test : ModuleRules
{
	public Programming_Test(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
