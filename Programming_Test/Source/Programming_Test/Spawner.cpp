// Fill out your copyright notice in the Description page of Project Settings.

#include "Programming_TestCharacter.h"
#include "Spawner.h"

// Sets default values
ASpawner::ASpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Creates the BoxComponent "Box" and sets it to the root component
	Box = CreateDefaultSubobject<UBoxComponent>(TEXT("Box"));
	RootComponent = Box;
	Box->SetHiddenInGame(false);
	Box->SetCollisionProfileName(TEXT("Trigger"));

	//Creates the SceneComponent "SpawnLocation" and attaches it to Box
	SpawnLocation = CreateDefaultSubobject<USceneComponent>(TEXT("SpawnLocation"));
	SpawnLocation->SetupAttachment(Box);

	
}

// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();

	//Adds implementation for when an actor begins overlap of the boxcomponent
	Box->OnComponentBeginOverlap.AddDynamic(this, &ASpawner::OnOverlapBegin);	
}

// Called every frame
void ASpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

//Causes a
void ASpawner::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) 
{
	if (OtherActor->GetClass()->IsChildOf(AProgramming_TestCharacter::StaticClass())) //Checks to see if the actor that is overlapping the player character
	{
		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;
		AActor* SpawnedActorRef = GetWorld()->SpawnActor<AActor>(SpawnSphere, SpawnLocation->GetComponentLocation(), SpawnLocation->GetComponentRotation(), SpawnParams); //Spawns the referenced actor
	}
}
