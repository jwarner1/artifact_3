// Copyright Epic Games, Inc. All Rights Reserved.

#include "Programming_TestGameMode.h"
#include "Programming_TestCharacter.h"
#include "UObject/ConstructorHelpers.h"

AProgramming_TestGameMode::AProgramming_TestGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
