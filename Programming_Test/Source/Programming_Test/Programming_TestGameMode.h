// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Programming_TestGameMode.generated.h"

UCLASS(minimalapi)
class AProgramming_TestGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AProgramming_TestGameMode();
};



