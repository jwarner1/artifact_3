Hello Reviewer,

Naming of elements within the environment have been matched according to the guidelines provided in the problem.

Upon entering the PIE, the spawners will be labeled Spawner1, Spawner2, and Spawner3 accordingly.

Spawner1:
 -Located front right from the player's starting position
 -Spawns SphereV1

Spawner2:
 -Located directly to the left of the player's starting position
 -Spawns SphereV2

Spawner3:
 -Located front left from the player's starting position
 -Spawns SphereV3

In addition, the type of sphere spawning at any specified spawner can be observed from the editor under the catagory "Spawning".

Thank you for your time,
Jacob Warner